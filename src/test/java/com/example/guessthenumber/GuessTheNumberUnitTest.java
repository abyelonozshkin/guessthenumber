/*
 * Copyright (c) 2018 Nextiva, Inc. to Present.
 * All rights reserved.
 */
package com.example.guessthenumber;

import reactor.core.publisher.Flux;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.reactive.socket.client.ReactorNettyWebSocketClient;
import org.springframework.web.reactive.socket.client.WebSocketClient;

import static org.hamcrest.CoreMatchers.is;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GuessTheNumberUnitTest {

    @Configuration
    @Import(GuessTheNumberApplication.class)
    public static class Config {

        @Bean
        public NumberService numberService() {
            return new TestNumberService();
        }
    }

    @LocalServerPort
    int port;

    @Test
    public void oneClientShouldWin() {
        int rounds = 2;
        WebSocketClient client = new ReactorNettyWebSocketClient();
        WSTestClient.client(client, port, rounds, is("You win"))
                .then()
                .block();
    }

    @Test
    public void oneClientShouldLose() {
        int rounds = 2;
        WebSocketClient client = new ReactorNettyWebSocketClient();
        Flux<Integer> generator = TestNumberService.incrementalGenerator().skip(1);
        WSTestClient.client(client, port, rounds, () -> generator, is("You lose"))
                .then()
                .block();
    }

    @Test
    public void multipleClientsShouldWinSimultaneously() {
        int clientNumber = 2;
        int rounds = 2;
        WebSocketClient client = new ReactorNettyWebSocketClient();
        Flux.mergeSequential(WSTestClient.clients(client, port, clientNumber, rounds, is("You win")))
                .then()
                .block();
    }
}

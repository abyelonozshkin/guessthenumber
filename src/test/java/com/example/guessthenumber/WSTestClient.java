/*
 * Copyright (c) 2018 Nextiva, Inc. to Present.
 * All rights reserved.
 */
package com.example.guessthenumber;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.hamcrest.Matcher;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.client.WebSocketClient;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class WSTestClient {

    public static List<Mono<Void>> clients(WebSocketClient client, int port, int clientNumber, int rounds, Matcher<String> resultMatcher) {
        return clients(client, port, clientNumber, rounds, () -> new TestNumberService().generator(), resultMatcher);
    }

    public static List<Mono<Void>> clients(WebSocketClient client, int port, int clientNumber, int rounds, Supplier<Flux<Integer>> generatorSupplier, Matcher<String> resultMatcher) {
        return IntStream.of(clientNumber)
                .mapToObj(i -> client(client, port, rounds, generatorSupplier, resultMatcher))
                .collect(Collectors.toList());
    }

    public static Mono<Void> client(WebSocketClient client, int port, int rounds, Matcher<String> resultMatcher) {
        return client(client, port, rounds, () -> new TestNumberService().generator(), resultMatcher);
    }

    public static Mono<Void> client(WebSocketClient client, int port, int rounds, Supplier<Flux<Integer>> generatorSupplier, Matcher<String> resultMatcher) {
        Flux<Integer> generator = generatorSupplier.get();
        return client.execute(
                URI.create("ws://localhost:" + port + "/game"),
                session -> session
                        .send(generator.next()
                                .map(Object::toString)
                                .log()
                                .map(session::textMessage))
                        .thenMany(session
                                .receive()
                                .take(2 * rounds)
                                .map(WebSocketMessage::getPayloadAsText)
                                .buffer(2)
                                .doOnNext(reply -> {
                                            assertThat(reply.get(0), resultMatcher);
                                            assertThat(reply.get(1), is("Start round"));
                                        }
                                )
                                .flatMap(ignored -> session
                                        .send(generator.next()
                                                .map(Object::toString)
                                                .map(session::textMessage))))
                        .then());
    }
}

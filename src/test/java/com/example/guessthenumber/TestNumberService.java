/*
 * Copyright (c) 2018 Nextiva, Inc. to Present.
 * All rights reserved.
 */
package com.example.guessthenumber;

import reactor.core.publisher.Flux;

import java.util.concurrent.atomic.AtomicInteger;

class TestNumberService implements NumberService {
    private Flux<Integer> generator = incrementalGenerator();

    public static Flux<Integer> incrementalGenerator() {
        return Flux.generate(AtomicInteger::new,
                (state, sunk) -> {
                    int number = state.incrementAndGet();
                    sunk.next(number);
                    return state;
                });
    }

    @Override
    public Flux<Integer> generator() {
        return generator.log();
    }
}

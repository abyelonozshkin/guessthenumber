/*
 * Copyright (c) 2018 Nextiva, Inc. to Present.
 * All rights reserved.
 */
package com.example.guessthenumber;

import reactor.core.publisher.Flux;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.reactive.socket.client.ReactorNettyWebSocketClient;
import org.springframework.web.reactive.socket.client.WebSocketClient;

import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.is;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GuessTheNumberIntegrationTest {

    @LocalServerPort
    int port;

    @Test
    public void testOneClient() {
        int rounds = 2;
        WebSocketClient client = new ReactorNettyWebSocketClient();
        WSTestClient.client(client, port, rounds, anyOf(is("You win"), is("You lose")))
                .then()
                .block();
    }

    @Test
    public void testManyClient() {
        int clientNumber = 2;
        int rounds = 2;
        WebSocketClient client = new ReactorNettyWebSocketClient();
        Flux.mergeSequential(WSTestClient.clients(client, port, clientNumber, rounds, anyOf(is("You win"), is("You lose"))))
                .then()
                .block();
    }
}

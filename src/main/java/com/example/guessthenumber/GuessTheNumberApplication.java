package com.example.guessthenumber;

import java.util.HashMap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.reactive.HandlerMapping;
import org.springframework.web.reactive.handler.SimpleUrlHandlerMapping;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.server.support.WebSocketHandlerAdapter;

@SpringBootApplication
public class GuessTheNumberApplication {

    public static void main(String[] args) {
        SpringApplication.run(GuessTheNumberApplication.class, args);
    }

    @Bean
    public HandlerMapping webSocketMapping(WSHandler wsHandler) {
        SimpleUrlHandlerMapping mapping = new SimpleUrlHandlerMapping();
        HashMap<String, WebSocketHandler> mappings = new HashMap<>();
        mappings.put("/game", wsHandler);
        mapping.setUrlMap(mappings);
        mapping.setOrder(1);
        return mapping;
    }

    @Bean
    public WebSocketHandlerAdapter handlerAdapter() {
        return new WebSocketHandlerAdapter();
    }
}

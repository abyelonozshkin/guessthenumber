/*
 * Copyright (c) 2018 Nextiva, Inc. to Present.
 * All rights reserved.
 */
package com.example.guessthenumber;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import org.springframework.stereotype.Service;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.WebSocketSession;

@Service
public class WSHandler implements WebSocketHandler {
    private final Flux<Integer> numberGenerator;

    public WSHandler(NumberService numberService) {
        this.numberGenerator = numberService.generator().log();
    }

    @Override
    public Mono<Void> handle(WebSocketSession session) {
        return session.send(
                session.receive()
                        .map(WebSocketMessage::getPayloadAsText)
                        .map(Integer::parseInt)
                        .flatMap(guess -> numberGenerator.next()
                                .log()
                                .map(number -> number.equals(guess)))
                        .map(win -> win ? "You win" : "You lose")
                        .concatMap(g -> Flux.just(g, "Start round"))
                        .map(session::textMessage));
    }
}
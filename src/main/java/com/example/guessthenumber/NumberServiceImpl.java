/*
 * Copyright (c) 2018 Nextiva, Inc. to Present.
 * All rights reserved.
 */
package com.example.guessthenumber;

import reactor.core.publisher.Flux;

import java.security.SecureRandom;
import java.time.Duration;

import org.springframework.stereotype.Service;

@Service
public class NumberServiceImpl implements NumberService {

    private final SecureRandom random = new SecureRandom();

    private final Flux<Integer> randomFlux = Flux.generate(sink -> {
        int number = random.nextInt();
        sink.next(number);
    });

    private Flux<Integer> randomDelayedMulticast = randomFlux
            .delayElements(Duration.ofSeconds(10))
            .publish()
            .autoConnect();

    @Override
    public Flux<Integer> generator() {
        return randomDelayedMulticast;
    }
}
